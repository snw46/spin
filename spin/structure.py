'''
This file defines the local structure ("gamma-matrices")
for spinor computations

At various points, we'd like to think of this structure as forms, as elements of a Clifford algebra,
or as matrices. The gamma-matrix implementation below provides various builtin conversion functions
like as_matrix, as_form, as_cliff to support these various interpretations.
These classes are smart, so will automatically try to pick the right form for a given context based
on a type inference.
'''

# pylint: disable=import-error
from functools import reduce
from operator import add, mul
from itertools import combinations

from sage.symbolic.ring import SR
from sage.quadratic_forms.quadratic_form import DiagonalQuadraticForm
from sage.algebras.clifford_algebra import CliffordAlgebra
from sage.matrix.matrix_space import MatrixSpace
from sage.modules.free_module import VectorSpace
from sage.rings.integer import Integer

from .octonion import OctonionAlgebra


cl = CliffordAlgebra(DiagonalQuadraticForm(SR, [-1]*8), ['c0', 'c1','c2','c3','c4','c5','c6','c7'])
cl_gamma = CliffordAlgebra(DiagonalQuadraticForm(SR, [-1]*7), [ f'gamma{j}' for j in range(7) ])
ext = CliffordAlgebra(DiagonalQuadraticForm(SR, [0]*8), ['e0', 'e1','e2','e3','e4','e5','e6','e7'])

# space of 8x8 matrices in SR
m8 = MatrixSpace(SR, 8)
v8 = VectorSpace(SR, 8)

class Spin7Structure:
    '''
    a Spin7Structure holds a Clifford algebra of gamma-matrices
    '''
    class Element:
        '''
        an element class for the Clifford algebra of gamma-matrices
        '''
        def __init__(self, _elt, parent):
            self._elt = _elt
            self._parent = parent

        def __repr__(self):
            try:
                return self.e().__repr__()
            except ValueError:
                return self._elt.__repr__()

        def __neg__(self):
            return Spin7Structure.Element(-self._elt, self._parent)

        def __sub__(self, other):
            if self._parent is not other._parent:
                raise ValueError('Cannot subtract gammas from distinct spin structures')
            return Spin7Structure.Element(self._elt - other._elt, self._parent)

        def __add__(self, other):
            if self._parent is not other._parent:
                raise ValueError('Cannot add gammas from distinct spin structures')
            return Spin7Structure.Element(self._elt + other._elt, self._parent)

        def __mul__(self, other):
            if self._parent is not other._parent:
                raise ValueError('Cannot multiply gammas from distinct spin structures')
            return Spin7Structure.Element(self._elt * other._elt, self._parent)

        def __rmul__(self, other):
            return Spin7Structure.Element(other * self._elt, self._parent)

        def __call__(self, x):
            '''
            act on elements of a free module as usual matrix multiplication
            e.g. if self = e01 + e23 + e45 + e67 and x = e7
            then the output is -e6
            '''
            alg = x.parent()
            l = [0] * 8
            for bs, c in list(x):
                if len(bs) != 1:
                    raise ValueError('call syntax supported only on pure grade 1 elements')
                l[list(bs)[0]] += c
            return reduce(add, (c*g for c, g in zip(self.m().transpose() * v8(l), alg.gens())))

        def parent(self):
            '''
            Return the Spin7Structure to which this element belongs
            '''
            return self._parent

        def c(self):
            '''
            return this element as an endomorphism of the algebra of spinors;
            that is, an element of the Clifford algebra of spinors
            '''
            return _matrix_to_clifford(cl, self.m(strict=True))

        def e(self):
            '''
            return this element as a differential form
            throws if not skew-symm
            '''
            return _matrix_to_clifford(ext, self.m(strict=True))

        def m(self, force_skew=False, strict=False):
            '''
            return this element as a matrix.
            throws if self is not skew-symm and strict=True
            automatically skew-symmetrizes if force_skew=True
            '''
            if force_skew:
                raise ValueError('forcing skew-symmetrization while in strict mode')

            m = self.parent().unpack(self, strict)
            if force_skew:
                return Integer(1)/2 * (m - m.transpose())
            return m

        def decompose(self):
            '''
            it is possible to write any element as a combination of a skew part,
            a traceless symmetric part, and a trace part.
            The skew-part corresponds to gamma(i) and gamma(i,j), the traceless symmetric
            to gamma(i,j,k) and the trace part to 1.
            This function decomposes any element in terms of these three components.
            The output is a triple: (a, b, c, d) where
            a is the trace
            b is a decomposition of the traceless symm part, given as a list of pairs ((i,j,k), c)
            where i < j < k and gamma^{ijk} appears with coeffcient c in the decomposition
            (c, d) is a decomposition of the skew-symm part; c is given as a list of pairs (i,c)
            where gamma^i appears with coefficient c, and d is given as a list of pairs ((i,j), c)
            where i < j and gamma^{ij} appears with coefficient d in the decomposition
            '''
            trace = self.m().trace()
            skew = Integer(1)/2 * (self.m() - self.m().transpose())
            traceless_sym = self.m() - skew - trace * self.m().parent().identity_matrix()
            gamma_length = Integer(8)
            gamma7_part = []
            gamma21_part = []
            gamma35_part = []
            for i in range(7):
                ip = -(skew * self.parent().gamma(i).m()).trace()
                if ip != 0:
                    gamma7_part.append((i, ip/gamma_length))
                for j in range(i, 7):
                    ijp = -(skew * self.parent().gamma(i,j).m()).trace()
                    if ijp != 0:
                        gamma21_part.append(((i,j), ijp/gamma_length))

                    for k in range(j,7):
                        ijkp = (traceless_sym * self.parent().gamma(i,j,k).m()).trace()
                        if ijkp != 0:
                            gamma35_part.append(((i,j,k), ijkp/gamma_length))

            return trace, gamma35_part, gamma7_part, gamma21_part

        def latex(self):
            '''
            convert a clifford algebra element to a latex representation
            '''
            raise NotImplementedError()

    def __init__(self, gamma_7):
        self._gens = [ Spin7Structure.Element(g, parent=self) for g in cl_gamma.gens() ]
        self._gamma_7 = gamma_7

    def unpack(self, elt, strict=True):
        '''
        Given a Spin7Structure.Element, unpack the underlying cl_gamma element,
        and convert to a matrix using the representation given by self._gamma_7.
        '''
        # pylint: disable=protected-access
        if elt.parent() is not self:
            raise ValueError('trying to unpack Element from a different Spin7Structure')

        mtx = m8.matrix(
                reduce(sum, (
                        (c * reduce(mul, (self._gamma_7[i] for i in bs)) if len(bs) > 0
                        else c * m8.identity_matrix())
                        for bs, c in list(elt._elt))))

        if strict and mtx != -mtx.transpose():
            raise ValueError('unpacking a non skew operator in strict mode')
        return mtx

    def __repr__(self):
        return f'Spin7Structure with gamma7 {[ x.e() for x in self._gens ]}'

    def gen(self, i):
        '''
        the i'th generator
        '''
        return self._gens[i]

    def gens(self):
        '''
        return a list of all generators
        '''
        return self._gens

    def gamma(self, *args):
        '''
        return the gamma element associated to the product
        '''
        return reduce(mul, (self._gens[i] for i in args))

    def gammas(self, grade):
        '''
        return a list of gammas of pure grade
        for example, gammas(1) is the list of gamma^i,
        gammas(2) is the list of gamma^{ij}, etc.
        '''
        return [ self.gamma(*x) for x in combinations(range(7), grade) ]

    def cayley(self):
        '''
        return the Cayley form
        '''
        return Integer(-1)/6 * reduce(add, (g.e() * g.e() for g in self.gammas(1)))

def _matrix_to_clifford(alg, matrix):
    '''
    convert an antisymmetric matrix to a clifford algebra element
    '''
    if matrix.transpose() + matrix != 0:
        raise ValueError('input matrix was not skew')

    return Integer(1)/2 * sum(sum(matrix[i][j]*alg.gen(i)*alg.gen(j) for j in range(8))
                              for i in range(8))

def gamma_7_octonion_left():
    '''
    standard gamma_7 structure associated to an octonion algebra's
    right-multiplication matrices
    '''
    octo = OctonionAlgebra(SR)
    return [ o.left_matrix() for o in octo.gens()[1:] ]

def gamma_7_octonion_right():
    '''
    standard gamma_7 structure associated to an octonion algebra's
    right-multiplication matrices
    '''
    octo = OctonionAlgebra(SR)
    return [ o.right_matrix() for o in octo.gens()[1:] ]

spin_7_octonion_left = Spin7Structure(gamma_7_octonion_left())
spin_7_octonion_right = Spin7Structure(gamma_7_octonion_right())
