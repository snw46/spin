'''
this module implements the local differential algebra of positive spinors in Spin(7)
we use the convention that an element in the 7-part of the spinors may be written as
c(gamma^m) psi_m for elements psi_m in the 1-part of the spinors
thus if psi = c(gamma^m) psi_m, it holds that psi_m = -1/16 c(gamma^m) psi.
'''

from functools import reduce
from operator import add, mul
from collections import defaultdict
import re

from sage.symbolic.ring import SR
from sage.algebras.free_algebra import FreeAlgebra

from .structure import ext

class Spin7DifferentialAlgebra:
    '''
    Spin7DA is the parent class for a 'differential algebra' of the positive spinors
    this class is intended to facilitate computations involving derivatives of
    spinors, and how they interact with curvatures
    '''
    class Element:
        '''
        a wrapper class for an element of a Spin7DifferentialAlgebra
        holds an element of a FreeAlgebra
        '''
        def __init__(self, elt, parent):
            self._elt = elt
            self._parent = parent

        def parent(self):
            '''
            return parent Spin7DifferentialAlgebra to which this element belongs
            '''
            return self._parent

        def __add__(self, other):
            return Spin7DifferentialAlgebra.Element(self._elt + other._elt, self._parent)

        def __sub__(self, other):
            return Spin7DifferentialAlgebra.Element(self._elt - other._elt, self._parent)

        def __mul__(self, other):
            return Spin7DifferentialAlgebra.Element(self._elt * other._elt, self._parent)

        def __rmul__(self, other):
            return Spin7DifferentialAlgebra.Element(other*self._elt, self._parent)

        def _group_like_terms(self):
            '''
            simplify an element by grouping on psi_i
            '''
            # pylint: disable=protected-access,unnecessary-lambda
            grouped = defaultdict(self.parent()._algebra.zero)

            for term, c in list(self._elt):
                # because of the variable ordering, psi is guaranteed to be some psi_i
                # unless term contains no psi; in this case, _group_like_terms is UB
                psi = list(term)[-1][0]

                # the naive term is monoidal in nature; if c < 0 this causes errors.
                # we should rewrite this term as an algebra element
                mon = reduce(mul, (x[0] ** x[1] for x in list(term)[:-1]))
                alg = self.parent()._algebra.Element(self.parent()._algebra, mon)

                grouped[psi] += c*alg

            #print(grouped)
            return grouped

        def _curvature_rewrite(self):
            '''
            rewrite
            '''

        # set some psi's to 0
        def eliminate(self,xs):
            return Spin7DifferentialAlgebra.Element(self._elt.substitute(**{self.parent().psi(i).__repr__(): 0 for i in xs}), self.parent())

        def __repr__(self):
            # terms containing only nabla or only psi should repr using the _algebra repr
            if not ('psi' in self._elt.__repr__() and 'nabla' in self._elt.__repr__()):
                return self._elt.__repr__()

            grouped = self._group_like_terms()
            serialized = ' + '.join(f'({v}) * {k}' for k, v in grouped.items())
            # rewrite curvature terms
            '''
            for i in range(1, 8):
                serialized = re.sub(re.escape(f'({self.parent().gamma(i)})'),
                                    f'Fg{i}', serialized)
                serialized = re.sub(re.escape(f'({-1*self.parent().gamma(i)})'),
                                    f'-Fg{i}', serialized)
                for j in range(i+1,8):
                    serialized = re.sub(re.escape(f'({self.parent().gamma(i,j)})'),
                                        f'Fg{i}{j}', serialized)
                    serialized = re.sub(re.escape(f'({-1*self.parent().gamma(i,j)})'),
                                        f'-Fg{i}{j}', serialized)
            '''
            return serialized

    def __init__(self, structure):
        self._algebra = FreeAlgebra(SR, 15, '''psi_0,psi_1,psi_2,psi_3,psi_4,psi_5,psi_6,
                                               nabla_0,nabla_1,nabla_2,nabla_3,
                                               nabla_4,nabla_5, nabla_6,nabla_7''')
        self._gens = [ Spin7DifferentialAlgebra.Element(g, self) for g in self._algebra.gens() ]
        self.s = structure

    def __repr__(self):
        return f'Spin7DifferentiaAlgebra with structure {self.s}'

    def _gen_to_ind(self, g):
        '''
        given a free algebra generator, return the index of the associated wrapped generator
        satisfies _gen_to_ind(self._gens[i]._elt) == i
        for internal use in printing Spin7DifferentialAlgebra.Element
        '''
        # pylint: disable=protected-access
        for i, gen in enumerate(self._gens):
            if gen._elt == g:
                return i
        raise KeyError(f'{g} is not a generator of {self}')

    def _ext_to_nabla(self, x):
        '''
        convert an exterior algebra element to a nabla expression
        e.g. _ext_to_nabla(e1) = nabla1, _ext_to_nabla(e8-e4) = nabla8 - nabla4, etc.
        '''
        return reduce(add, (c * reduce(mul, (self.nabla(x) for x in list(bs))) for bs, c in x))

    def zero(self):
        '''
        the zero element of the algebra
        '''
        return Spin7DifferentialAlgebra.Element(self._algebra.zero(), self)

    def nabla(self, ind):
        '''
        returns the i'th partial derivative operator
        '''
        if ind < 0 or ind > 7:
            raise ValueError(f'index {ind} on nabla is not valid; should be between 0 and 7')
        return self.nablas()[ind]

    def nablas(self):
        '''
        returns list of all nabla
        '''
        return self._gens[7:]

    def psi(self, ind):
        '''
        returns the i'th spinorial component
        '''
        if ind < 0 or ind > 6:
            raise ValueError(f'index {ind} on psi is not valid; should be between 0 and 6')

        return self.psis()[ind]

    def psis(self):
        '''
        list of all psis
        '''
        return self._gens[:7]

    def contract(self, gamma, a):
        '''
        contract nablas along gamma_a
        computes gamma_{ab} nabla_{b} = nabla_{gamma e_a}
        thus, this function is equivalent to _ext_to_nabla(gamma(ext.gen(a)))
        '''
        return self._ext_to_nabla(gamma(ext.gen(a)))


    def D7(self, a):
        '''
        compute D7(a) psi_m = nabla_{gamma^m e_a} psi_m
        on a Spin7 manifold, this operator vanishes
        '''
        return reduce(add, (self.contract(gamma, a) * psi
                      for gamma, psi in zip(self.s.gammas(1), self.psis())))

    def D21(self, m, a):
        '''
        compute D21(a) psi_m = c(gamma^jm) nabla_{gamma^jm e_a} psi
        on a Spin7 manifold, this operator is equal to nabla_a
        this is obtained by contracting D7_a along gamma^m
        '''
        return reduce(add, (self.contract(self.s.gamma(m, i), a) * psi
                            for i, psi in enumerate(self.psis())))

    def D35(self, k, m, a):
        '''
        compute D35(a) psi_m, a triple-gamma contraction of the connection
        '''
        return reduce(add, (self.contract(self.s.gamma(k, m, i), a) * psi
                            for i, psi in enumerate(self.psis())))

    def rewrite_gamma(self, elt):
        '''
        rewrite an element of self.s as a nabla-expression
        e.g. if gamma0 = e01 + e23 + e45 + e67
        then rewrite_gamma(gamma0) is nabla0*nabla1 - nabla1*nabla0 + ...
        '''
        ans = self.zero()
        for i, row in enumerate(elt.m()):
            for j, el in enumerate(row):
                ans += el * self.nabla(i) * self.nabla(j)
        return ans

    def rewrite_nabla(self, elt):
        '''
        rewrite an element purely containing nablas in terms of gammas
        e.g. rewrite_nabla(nabla0*nabla1 - nabla1*nabla0 + ...) = gamma0
        '''
        raise NotImplementedError()

    def latex(self, term):
        '''
        rewrite an algebra term as a latex expression
        '''
        latex_repr = re.sub('nabla', '\\\\nabla', str(term))
        latex_repr = re.sub('psi', '\\\\psi', latex_repr)
        latex_repr = re.sub('\\*', ' ', latex_repr)
        latex_repr = re.sub(r'([a-z])([0-9])', r'\1_\2', latex_repr)
        return latex_repr
