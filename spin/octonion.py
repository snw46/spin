'''
Class implementing the octonions via the Cayley--Dickson construction
'''

from functools import reduce

from sage.functions.other import conjugate
from sage.rings.integer import Integer
from sage.symbolic.ring import SR
from sage.algebras.quatalg.quaternion_algebra import QuaternionAlgebra
from sage.matrix.matrix_space import MatrixSpace

class Octonion:
    '''
    an Octonion is a pair of quaternions with multiplication
    given by the Cayley--Dickson construction
    '''
    def __init__(self,a,b, parent, names=('e0', 'e1', 'e2', 'e3',
                                          'e4', 'e5', 'e6', 'e7')):
        self.a = a
        self.b = b
        self._names = names
        self._parent = parent

    def __add__(self, other):
        return Octonion(self.a+other.a, self.b+other.b,
                        self._parent, names=self._names)

    def __sub__(self, other):
        return Octonion(self.a-other.a, self.b-other.b,
                        self._parent, names=self._names)

    def __rmul__(self, other):
        return Octonion(self.a*other, self.b*other,
                        self._parent, names=self._names)

    def __mul__(self, other):
        '''
        Cayley--Dickson multiplication:
        (a,b)(c,d) = (ac - b* d, da + b c*)
        '''
        return Octonion(self.a*other.a - conjugate(other.b)*self.b,
                        other.b*self.a + self.b*conjugate(other.a),
                        self._parent, names=self._names)

    def __list__(self):
        return list(self.a) + list(self.b)

    def __getitem__(self, key):
        if key < 0 or key > 7:
            raise KeyError(f'index {key} is not in [0,7]')
        return self.__list__()[key]

    def __repr__(self):
        serialized = ''
        for i, c in enumerate(self.__list__()):
            if c == 0:
                continue

            if serialized == '':
                if c == 1:
                    serialized += f'{self._names[i]}'
                elif c == -1:
                    serialized += f'-{self._names[i]}'
                else:
                    serialized += f'{c}*{self._names[i]}'
            elif c == 1:
                serialized += f' + {self._names[i]}'
            elif c == -1:
                serialized += f' - {self._names[i]}'
            elif c > 0:
                serialized += f' + {c}*{self._names[i]}'
            elif c < 0:
                serialized += f' - {-c}*{self._names[i]}'
        if serialized == '':
            serialized = '0'
        return serialized

    def __eq__(self, other):
        return self.a == other.a and self.b == other.b

    def conjugate(self):
        '''
        conjugate octonion of (a, b) is (a*, -b)
        '''
        return Octonion(conjugate(self.a), -self.b,
                        self._parent, names=self._names)

    def real(self):
        '''
        return the real part of the octonion
        '''
        return self.a[0]

    def imag(self):
        '''
        imaginary part of an octonion
        '''
        return Integer(1)/2 * (self - self.conjugate())

    def parent(self):
        '''
        return the parent OctonionAlgebra
        to which this octionion belongs
        '''
        return self._parent

    def left_matrix(self):
        '''
        the octonion multiplication on the left is an endomorphism
        of the vector space of octonions; this is the 8x8 matrix
        associated to that left-multiplication map
        '''
        MS = MatrixSpace(self.parent().ring, 8)
        return MS.matrix([ (self * other).__list__() for other in self.parent().gens() ])

    def right_matrix(self):
        '''
        as left_matrix, but with right-multiplication
        '''
        MS = MatrixSpace(self.parent().ring, 8)
        return MS.matrix([ (other * self).__list__() for other in self.parent().gens() ])

    def dot(self, other):
        '''
        the octonion inner product
        '''
        return (self.conjugate() * other).real()

class OctonionAlgebra:
    '''
    non-associative algebra of octonions over a ring
    '''
    def __init__(self, ring=SR, names=('e0', 'e1', 'e2', 'e3',
                                       'e4', 'e5', 'e6', 'e7')):
        self.ring = ring
        self.qa = QuaternionAlgebra(ring, -1, -1)
        qi, qj, _ = self.qa.gens()
        qz = 0 * qi
        i = Octonion(qi, qz, self, names)
        j = Octonion(qj, qz, self, names)
        l = Octonion(qz, -qi*qi, self, names)
        e = Octonion(-qi*qi, qz, self, names)
        self._gens = (
            e, i, j, i*j,
            l, i*l, j*l, (i*j)*l
        )

    def __repr__(self):
        return f'Octonion algebra over {self.ring}'

    def _first_ngens(self, n):
        return self._gens[:n]

    def gen(self, ind):
        '''
        return the generator of the octonion algebra of the given index
        '''
        if ind < 0 or ind > 7:
            raise KeyError(f'index {ind} is not in [0,7]')
        return self._gens[ind]

    def gens(self):
        '''
        return a list of generators of the octonion algebra
        '''
        return self._gens

    def from_left_matrix(self, matrix):
        '''
        given a matrix, return the octonion o for which o.left_matrix() == matrix
        raises ValueError if no such matrix exists
        '''
        o = reduce(lambda a, b: a + b, (a * b for a, b in zip(matrix[0], self.gens())))
        if o.left_matrix() != matrix:
            raise ValueError(f'{matrix} is not the left-multiplication matrix of an octonion')
        return o

    def from_right_matrix(self, matrix):
        '''
        given a matrix, return the octonion o for which o.left_matrix() == matrix
        raises ValueError if no such matrix exists
        '''
        o = reduce(lambda a, b: a + b, (a * b for a, b in zip(matrix[0], self.gens())))
        if o.right_matrix() != matrix:
            raise ValueError(f'{matrix} is not the right-multiplication matrix of an octonion')
        return o
